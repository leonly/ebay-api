'''
@Descripttion: ebayapi
@version: v1.0
@Author: Leo
@Date: 2020-04-14 10:56:38
@Last Modified by: Leo
@Last Modified time: Do not Edit
'''

from concurrent.futures import ThreadPoolExecutor
from ebay_api_spider import dealWithSearchWord, loadConfig, flushJsonData
import time


def run(checkConfig, config, runState):
    maxThreadNum = 4
    if 'maxThreadNum' in config:
        maxThreadNum = config['maxThreadNum']
    pool = ThreadPoolExecutor(max_workers=maxThreadNum)

    market = ["EBAY_US"]
    if 'market' in config:
        market = config['market']

    if checkConfig:
        marketIndex = runState['marketIndex']
        for i in range(marketIndex, len(market)):
            mark = market[i]

            runState['market'] = mark
            runState['marketIndex'] = i

            searchWords = []
            if 'searchWords' in config:
                searchWords = config["searchWords"]

            if checkConfig:
                target = dealWithSearchWord(mark, checkConfig)
                minIndex = runState['searchWordMinIndex']
                maxIndex = runState['searchWordMaxIndex']
                if minIndex == 0 and maxIndex == 0:
                    maxIndex = maxThreadNum - 1
                if maxIndex <= len(searchWords) and minIndex < len(searchWords):
                    for d in pool.map(target, searchWords[minIndex:maxIndex+1]):
                        pass
                else:
                    continue
                runState['searchWordMinIndex'] = maxIndex+1
                runState['searchWordMaxIndex'] = min(maxIndex + maxThreadNum, len(searchWords))
                flushJsonData("run_state.json", runState)
                checkConfig = False
                target = dealWithSearchWord(mark, checkConfig)
                k = 0
                for l in [searchWords[i:i+maxThreadNum] for i in range(maxIndex + 1, len(searchWords), maxThreadNum)]:
                    for d in pool.map(target, l):
                        pass
                    k += 1
                    runState['searchWordMinIndex'] = min(k * maxThreadNum + maxIndex + 1, len(searchWords))
                    runState['searchWordMaxIndex'] = min((k + 1) * maxThreadNum + maxIndex, len(searchWords))
                    flushJsonData("run_state.json", runState)
            else:
                k = 0
                target = dealWithSearchWord(mark, checkConfig)
                for l in [searchWords[i:i+maxThreadNum] for i in range(0, len(searchWords), maxThreadNum)]:
                    for d in pool.map(target, l):
                        pass
                    k += 1
                    runState['searchWordMinIndex'] = min(k * maxThreadNum, len(searchWords))
                    runState['searchWordMaxIndex'] = min((k + 1) * maxThreadNum - 1, len(searchWords))
                    flushJsonData("run_state.json", runState)
    else:
        if runState is None:
            runState = {}
        for i in range(len(market)):
            mark = market[i]
            target = dealWithSearchWord(mark, False)

            runState['market'] = mark
            runState['marketIndex'] = i

            searchWords = []
            if 'searchWords' in config:
                searchWords = config["searchWords"]

            k = 0
            for l in [searchWords[i:i+maxThreadNum] for i in range(0, len(searchWords), maxThreadNum)]:
                
                for d in pool.map(target, l):
                    pass
                k += 1
                runState['searchWordMinIndex'] = min(k * maxThreadNum, len(searchWords))
                runState['searchWordMaxIndex'] = min((k + 1) * maxThreadNum - 1, len(searchWords))
                flushJsonData("run_state.json", runState)

    pool.shutdown()


if __name__ == "__main__":
    loadConfig()
    from ebay_api_spider import config, run_state
    checkConfig = True
    while True:
        run(checkConfig, config, run_state)
        time.sleep(3600 * 1)
        checkConfig = False
