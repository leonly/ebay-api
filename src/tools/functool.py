#!/usr/bin/env python
# _*_ coding: utf-8 _*_
'''

Description: It's fun for you
Author: Leo
Date: 11/15/2019

'''

from functools import reduce
from pointfree import partial
import re


class SpiderFuctor(object):
    def __init__(self, f):
        self._value = f

    def isNone(self):
        return self._value is None

    def map(self, f):
        if self.isNone():
            return SpiderFuctor(None)
        return SpiderFuctor(compose(f, self._value)) if callable(
            self._value) else SpiderFuctor(f(self._value))

    def join(self):
        return self._value() if callable(self._value) else self._value

    def chain(self, f):
        return self.map(f).join()

    def ap(self, other_functor):
        return other_functor.map(self._value)


def compose(*funcs):
    def compose2(f, g, *orig_args, **orig_kwds):
        def nested_function(*more_args, **more_kwds):
            return f(g(*more_args, **more_kwds), *orig_args, **orig_kwds)
        return nested_function
    return reduce(compose2, funcs, lambda x: x)


# chain :: SpiderFuctor m => (a -> m b) -> m a -> m b
@partial
def fchain(f, m):
    return m.map(f).join()


# pfmap :: SpiderFuctor m => (a -> b) -> m a -> m b
@partial
def fmap(f, m):
    return m.map(f)


def fjoin(m):
    return m.join()


# mmap :: (a -> b) -> [a] -> Iter([b])
mmap = partial(lambda f, arr: map(f, arr))


freduce = partial(lambda f, b, arr: reduce(f, arr, b))


ffilter = partial(lambda f, arr: filter(f, arr))


# liftA2 :: SpiderFuctor m => (a b -> c) -> m a -> m b -> m c
@partial
def liftA2(f, functor1, functor2):
    return functor1.map(f).ap(functor2)


# liftA2 :: SpiderFuctor m => (a b c -> d) -> m a -> m b -> m c -> m d
@partial
def liftA3(f, functor1, functor2, functor3):
    return functor1.map(f).ap(functor2).ap(functor3)


@partial
def liftA4(f, functor1, functor2, functor3, functor4):
    return functor1.map(f).ap(functor2).ap(functor3).ap(functor4)


# con :: a -> b -> ab
@partial
def con(s1, s2):
    return f'{s1}{s2}'


# con :: a -> b -> ab
@partial
def append(s1, s2):
    return f'{s2}{s1}'


@partial
def extend(arr1, arr2):
    arr1.extend(arr2)
    return arr1


@partial
def findAny(regx, group_num, s):
    res = re.search(regx, s)
    return res.group(group_num) if res is not None else None


def car(arr):
    return arr[0] if arr is not None and len(arr) > 0 else None


def cdr(arr):
    return arr[1:] if arr is not None and len(arr) > 0 else None


@partial
def findNumber(g_num, string):
    if string is None:
        return None
    return findAny('[-+]?[0-9]*,*[0-9]*\.?[0-9]+', g_num)(string)


@partial
def index(substr, s):
    try:
        return s.index(substr)
    except Exception as e:
        print(e)
        return 0


@partial
def substr(start, end, s):
    return s[start: end]


@partial
def substrI(s1, s2, s):
    return substr(index(s1, s) + len(s1), index(s2, s), s)


@partial
def replace(fromstr, tostr, s):
    return s.replace(fromstr, tostr)


@partial
def sub(regx, tostr, s):
    return re.sub(regx, tostr, s)


@partial
def prop(attr, obj):
    return obj[attr]


def filterEmpty(arr):
    return filter(lambda x: x is not None and x != [] and x != '', arr)


@partial
def splitList(n, l):
    for idx in range(0, len(l), n):
        yield l[idx:idx + n]


@partial
def fzip(iter1, iter2):
    return zip(iter1, iter2)


def trace(item):
    fmap(print, item)
    return item
