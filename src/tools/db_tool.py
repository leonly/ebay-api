#!/usr/bin/env python
# _*_ coding: utf-8 _*_

'''

Description:  DB tool
Author: Leo
Date: 12/27/2019

'''
from pymongo import MongoClient, InsertOne, ReplaceOne
from pointfree import partial
import logging
import socket


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)  # Log等级总开关

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)   # 输出到console的log等级的开关

formatter = logging.Formatter(
    "%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


# Not pure and I hate it, but I have no idea to deal with it
# Because I don't want to wrapper it with my functor
def getMongoClient():
    MONGODB_SERVER = '172.24.8.108'  # 172.24.8.108D
    if socket.gethostname() not in ["SPIDER-WEB", 'SPIDER-ADMIN']:
        logger.info("使用本地数据库")
        MONGODB_SERVER = '10.8.0.204'  # 172.24.8.108D
    MONGODB_PORT = 27017
    # db = MongoClient(host=MONGODB_SERVER, port=MONGODB_PORT).t2_Ebay
    db = MongoClient(host=MONGODB_SERVER, port=MONGODB_PORT).ScrapyDB
    logger.info("mongo数据库连接成功")
    return db


@partial
def findExsistLotByLotId(db, source, lot_id):
    res = db.Lots.find_one({'source': source, 'lot_id': lot_id}, {
                           'lot_id': 1, 'state': 1})
    if res is not None:
        if res['state'] == -99:
            return {'re': False, 'res': res}
        logger.info(f"数据重复， lot_id: {lot_id}")
        return {'re': True, 'res': res}
    return {'re': False, 'res': res}


@partial
def insertOneOnCheck(db, lot):
    # logger.info(lot['name'])
    e = findExsistLotByLotId(db, lot["source"], lot["lot_id"])
    if e['re']:
        return False
    db.Lots.insert(lot)
    logger.info("insert one: " + lot['lot_id'])
    return True


@partial
def bulkSave(db, items):
    if items is None:
        return {"inserted_count": 0, "upserted_count": 0}
    inserts = list(map(lambda k: InsertOne(
        k), filter(lambda t: "_id" not in t, items)))
    upserts = list(map(lambda k: ReplaceOne(
        {'_id': k['_id']}, k, upsert=True), filter(lambda t: "_id" in t, items)))
    inserts.extend(upserts)
    if len(inserts) < 1:
        return {"inserted_count": 0, "upserted_count": 0}
    re = db.Lots.bulk_write(inserts, ordered=False)
    return {"inserted_count": re.inserted_count, "upserted_count": re.upserted_count}