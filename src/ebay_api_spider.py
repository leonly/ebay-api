'''
@Descripttion: ebay api
@version: v1.0
@Author: Leo
@Date: 2020-04-13 11:28:13
@Last Modified by: Leo
@Last Modified time: Do not Edit
'''
import requests
from tools import functool as fun
from pointfree import partial
from tools import db_tool as mongo
from bs4 import BeautifulSoup
import math
import threading
import base64
import logging
import json
import time
import datetime
import hashlib


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)  # Log等级总开关

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)   # 输出到console的log等级的开关

formatter = logging.Formatter(
    "%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)
config = {}
run_state = {}
change = 0
mutex = threading.Lock()


def loadConfig():
    try:
        with open('ebay_config.json', 'r', encoding='utf-8') as f:
            c = f.read()
            global config
            config = json.loads(c)
            config['token'] = '123'
        with open('run_state.json', 'r', encoding='utf-8') as f:
            c = f.read()
            global run_state
            run_state = json.loads(c)
        requestEbayToken()
    except Exception as e:
        logger.error(e)


def loadByName(name):
    try:
        with open(name, 'r', encoding='utf-8') as f:
            c = f.read()
            cfg = json.loads(c)
        return cfg
    except Exception as e:
        logger.error(e)
        return None


def flushJsonData(name, data):
    with open(name, 'w', encoding='utf-8') as f:
        f.write(json.dumps(data))


def requestEbayToken(change):
    mutex.acquire()
    url = 'https://api.ebay.com/identity/v1/oauth2/token?grant_type=client_credentials&scope=https://api.ebay.com/oauth/api_scope'
    global config
    global change
    if config['token'] != "123":
        mutex.release()
        return
    if change:
        change = change + 1
    auth = config["auth"]
    size = len(auth)
    while True:
        if change > size:
            change = 0
        appid = auth[change]['appid']
        sertid = auth[change]['certid']
        bytedata = (appid + ":" + sertid).encode("utf-8")
        secret = str(base64.b64encode(bytedata), 'utf-8')
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + secret
        }
        try:
            logger.info(f"开始请求token: {config['token']}")
            res = requests.post(url, headers=headers)
            if res.ok and res.status_code == 200:
                config['token'] = res.json()['access_token']
                mutex.release()
                logger.info(f"token请求结束token: {config['token']}")
                return
            else:
                logger.error(f"请求token失败：{res.text}")
                change += 1
                if change >= size:
                    change = 0
        except Exception as e:
            logger.error(f"请求token异常：{e}")
            change += 1
            if change >= size:
                change = 0


@partial
def getEbaySearch(market, keyword, limit, offset):
    t, k = 0
    while True:
        token = "token"
        if 'token' in config:
            token = config['token']
        if not token.startswith("Bearer"):
            token = "Bearer " + token

            categoryids = ",".join(
                config["filterCategoryIds"]) if "filterCategoryIds" in config else "37919"
            filters = config[
                "filterOther"] if "filterOther" in config else "price:[1..10000],priceCurrency:USD,buyingOptions:{FIXED_PRICE},deliveryCountry:US"
            outSellers = "|".join(config["filterSellers"])
            filters = filters + ",excludeSellers:{" + outSellers + "}"

        headers = {
            "Authorization": token,
            "X-EBAY-C-MARKETPLACE-ID": market,
            "Content-Type": "application/json",
            "X-EBAY-C-ENDUSERCTX": 'contextualLocation=country=<2CharCountryCode>,zip=<5DigitCode>,affiliateCampaignId=<ePNCampaignId>,affiliateReferenceId=<referenceId>'
        }

        params = {
            "q": keyword,
            "category_ids": categoryids,
            "filter": filters,
            "limit": limit,
            "offset": offset
        }

        url = "https://api.ebay.com/buy/browse/v1/item_summary/search"

        try:
            # logger.info("开始请求搜索×××××××××××××××××××××××××××××")
            re = requests.get(url, headers=headers, params=params, timeout=120)
            if re.ok and re.status_code == 200:
                logger.info(f"搜索成功: {keyword}")
                t, k = 0
                return fun.SpiderFuctor(json.loads(re.text))
            logger.error("请求搜索失败: " + re.text)
            config['token'] = "123"
            time.sleep(3)
            if t > 3:
                requestEbayToken(True)
            else:
                requestEbayToken(False)
            t += 1
        except Exception as e:
            logger.error(f"搜索异常：{e}")
            k += 1
            if k > 3:
                break
    return fun.SpiderFuctor(None)


@partial
def getEbayItem(market, lotId):
    t = 0
    k = 0
    while True:
        token = "token"
        if 'token' in config:
            token = config['token']
        if not token.startswith("Bearer"):
            token = "Bearer " + token
        url = f"https://api.ebay.com/buy/browse/v1/item/{lotId}"
        headers = {
            "Authorization": token,
            "X-EBAY-C-MARKETPLACE-ID": market,
            "Content-Type": "application/json",
            "X-EBAY-C-ENDUSERCTX": "contextualLocation=country=<2CharCountryCode>,zip=<5DigitCode>,affiliateCampaignId=<ePNCampaignId>,affiliateReferenceId=<referenceId>"
        }
        try:
            # logger.info("开始请求详情***********************")
            re = requests.get(url, headers=headers, timeout=60)
            if re.ok and re.status_code == 200:
                logger.info(f"请求详情成功: {lotId}")
                t, k = 0
                return fun.SpiderFuctor(json.loads(re.text))
            logger.error("请求详情失败: " + re.text)
            config['token'] = "123"
            time.sleep(1)
            if t > 3:
                requestEbayToken(True)
            else:
                requestEbayToken(False)
            t += 1
        except Exception as e:
            logger.error(f"请求详情异常: {e}")
            k += 1
            if k > 3:
                break
    return fun.SpiderFuctor(None)


# 构造入库数据
@partial
def destructItem(auction_db_id, searchItem, item):
    data = {}
    t = time.time()
    try:
        start_time = datetime.datetime.now() + datetime.timedelta(days=30)
        if '_id' in searchItem:
            data['_id'] = searchItem['_id']
        data['lot_id'] = searchItem["itemId"].strip().split('|')[1]
        data['source'] = "ebay_now"
        data['state'] = -1
        data['create_time'] = t
        data['auction_shooting_local_time'] = start_time.strftime(
            '%Y-%m-%d %H:%M:%S')
        data['lot_no'] = str(math.ceil(t*1000) % 10000) + "5"
        data['url'] = searchItem['itemWebUrl']
        data['seller_name'] = searchItem['seller']['username']
        data["auction_db_id"] = auction_db_id
        data['auction_id'] = "bn_2313579654"
        data['auction_shooting_time'] = start_time.strftime(
            '%Y-%m-%d %H:%M:%S')
        data['name'] = searchItem['title']
        # unit = searchItem['price']['currency']
        unit = '£'
        price = float(searchItem['price']['value'])
        data['starting_bid'] = unit + str(price)
        # data['auction_id'] = auction_db_id,
        data['estimate'] = unit+str(price*2) + '-' + unit + str(price*4)
        data['detail_large_url'] = item['image']['imageUrl']
        data['thumbnail_url'] = item['image']['imageUrl']
        data['category_name'] = item['categoryPath']
        if 'shortDescription' in item:
            data['description'] = item['shortDescription']
        elif 'description' in item:
            soup = BeautifulSoup(item['description'], 'html.parser')
            data['description'] = soup.text
        else:
            data['description'] = item['title']

        def srcfun(a):
            a['src'] = a['imageUrl']
            a['fullsrc'] = a['imageUrl']
            a.pop('imageUrl')
            return a
        if 'additionalImages' in item:
            data['imags_url'] = list(map(srcfun, item['additionalImages']))
        elif 'primaryItemGroup' in item:
            gp = item['primaryItemGroup']
            if 'itemGroupImage' in gp:
                data['imags_url'] = list(map(srcfun, [gp['itemGroupImage']]))
            elif 'itemGroupAdditionalImages' in gp:
                data['imags_url'] = list(
                    map(srcfun, gp['itemGroupAdditionalImages']))
        else:
            data['imags_url'] = list(map(srcfun, [item['image']]))
        return data
    except Exception as e:
        logger.info("error occurs when dealing with item: " +
                    searchItem['itemId'])
        logger.error(e)
        return None


# 处理搜索出来的商品,包括处理和入库
@partial
def processSearchItem(auction_db_id, requestItem, searchItem):
    goodsId = searchItem['itemId']
    item = requestItem(goodsId)
    return fun.fchain(destructItem(auction_db_id, searchItem))(item)


@partial
def checkItem(db, source, searchItem):
    lot_id = searchItem["itemId"].strip().split('|')[1]
    # logger.info(lot_id)
    res = mongo.findExsistLotByLotId(db, source, lot_id)
    boo = res['re']
    if not boo:
        info = res['res']
        if info is not None:
            searchItem["_id"] = info['_id']
    return not boo


@partial
def dealWithSearchWord(market, checkConfig, searchWord):
    searchCount = 10000
    searchLimit = 200
    if "useSearchCount" in config:
        searchCount = config["useSearchCount"]
    if "useSearchLimit" in config:
        searchLimit = config["useSearchLimit"]
    searchOffset = getEbaySearch(market, searchWord, searchLimit)
    requestItem = getEbayItem(market)
    # md5 = hashlib.md5()
    # md5.update((searchWord + ':' + market).encode('utf-8'))
    # auction_db_id = md5.hexdigest()[0:24]
    auction_db_id = "5e9050d079eff598041394d3"
    db = mongo.getMongoClient()
    job = processSearchItem(auction_db_id, requestItem)
    checkEbay = checkItem(db, "ebay_now")
    bulkSave = mongo.bulkSave(db)
    offdata = {}

    offset = 0
    if checkConfig:
        cfg = loadByName(f"state/{searchWord}")
        if cfg is not None and searchWord in cfg:
            offset = cfg[searchWord]

    for i in range(offset, math.ceil(searchCount/searchLimit)):
        offdata[searchWord] = i
        flushJsonData(f"state/{searchWord}", offdata)
        list100 = searchOffset(i*searchLimit)
        fun.compose(fun.trace, fun.fmap(bulkSave), fun.fmap(fun.filterEmpty), fun.fmap(fun.mmap(
            job)), fun.fmap(fun.ffilter(checkEbay)), fun.fmap(fun.prop('itemSummaries')))(list100)

        if int(list100.join()['total']) < i*searchLimit + searchLimit:
            break
